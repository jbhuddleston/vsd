CONFIG.system.combat = [
  { action: "Select", initiative: 90 },
  { action: "Move", initiative: 61 },
  { action: "Spell A", initiative: 51 },
  { action: "Ranged A", initiative: 41 },
  { action: "Longest Melee Plus", initiative: 37 },
  { action: "Longest Melee", initiative: 36 },
  { action: "Long Melee", initiative: 35 },
  { action: "Medium Melee", initiative: 34 },
  { action: "Short Melee", initiative: 33 },
  { action: "Hand Melee", initiative: 32 },
  { action: "Hand Melee Minus", initiative: 31 },
  { action: "Ranged B", initiative: 21 },
  { action: "Spell B", initiative: 11 },
  { action: "Other Action", initiative: 1 },
  { action: "Wait - Ready", initiative: 0 }
];
