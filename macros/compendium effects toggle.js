/**
 * This will bring up a dialog containing the names of all the conditions in 
 * the named world compendium. They must all be modifiers that may be toggled 
 * and you use that dialog to toggle the effect on selected tokens.
 * 
 * This macro may only be used after the copy items from compendium macro has 
 * copied the conditions to the token actor.
 * 
 * This macro will then toggle the condition and icon on the selected tokens.
 */

const compendiumName = "conditions";

/**
 * @folderName is the case-sensitive name of the folder containing your conditions
 */
async function toggleFolderConditions(compendiumName) {
  const items = await game.packs.get(`world.${compendiumName}`)?.getDocuments() || [];
  if (items.length == 0) {
    ui.notifications.error(`The world compendium "${compendiumName}" does not exist or is empty.`);
    return;
  }

  const buttons = {};
  for (const button of items) {
    buttons[button.name] = {
      label: button.name,
      buttonData: button,
      callback: () => {
        (label = button.name), (icon = button.img), d.render(true);
      },
    };
  }

  let d = new Dialog(
    {
      title: `Toggle Status Effect`,
      buttons: buttons,
      close: async (html) => {
        if (icon) {

          const tokens = canvas.tokens.controlled;
          if (tokens.length == 0) {
            ui.notifications.error("Please select at least one token first");
            return;
          }
          for (const token of tokens) {
            const actor = token.actor;
            const itemdata = buttons[label].buttonData;

            const modref = CONFIG.system.slugify(itemdata.name);
            let existing = actor.system.modifiers?.[modref] || null;
            if (existing == null) {
              ui.notifications.error(`There is no matching item for "${itemdata.name}" on Token:${token.name}.`);
              return;
            }
            const isOn = existing.system.inEffect;
            await existing.update({ system: { inEffect: !isOn, alwaysOn: false } });
            await token.document.toggleActiveEffect({
              icon: icon,
              id: modref,
              label: label,
            });
                  }
          (label = ""), (icon = "");
        }
      },
    },
    {
      width: 200,
      classes: ["mydialog"],
      top: 0,
      left: 0,
    }
  );
  d.render(true);
}
toggleFolderConditions(compendiumName);
console.log(compendiumName);