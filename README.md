# Against-the-Darkmaster

Official Foundry Virtual TableTop system for the Against the Darkmaster RPG by Open Ended Games.

<blockquote>
<b>Against the Darkmaster</b> is a tabletop Epic Fantasy roleplaying game of high adventure, heroic action, and heavy metal combat.

If you put together works like The <i>Lord of the Rings</i> and <i>The Wheel of Time</i>, sprinkled them with a bit of <i>Labyrinth</i> and <i>Dragonslayer</i>, and put everything in a blender together with a healthy dose of classic Heavy Metal, you’ll get a typical Against the Darkmaster game session.

Travel to distant lands, face terrible dangers, uncover ancient items of power, and gather the armies of the world to defeat the ultimate Evil.
</blockquote>

Visit https://www.vsdarkmaster.com/ to check out the amazing range of new products by Open Ended Games.

Join the Against the Darkmaster Discord server here; https://discord.gg/uXPCjrH, and my Discord server for more detailed development or background information here; https://discord.gg/d8ujbNG.

Please do buy me a coffee if you like what you see, here; https://ko-fi.com/jbhuddleston. Thank you.
