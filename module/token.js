export class VsDTokenDocument extends TokenDocument {

  /**
   * A helper method to retrieve the underlying data behind one of the Token's attribute bars
   * @param {string} barName        The named bar to retrieve the attribute for
   * @param {string} alternative    An alternative attribute path to get instead of the default one
   * @return {object|null}          The attribute displayed on the Token bar, if any
   */
  getBarAttribute(barName, { alternative } = {}) {
    const attr = alternative || this[barName]?.attribute;
    if (!attr || !this.actor) return null;
    let data = foundry.utils.getProperty(this.actor.system, attr);
    if (data === null || data === undefined) return null;

    // Single values but we are not supporting single values
    if (Number.isNumeric(data)) {
      return {
        type: "value",
        attribute: attr,
        value: Number(data),
        editable: true, // super requires a templated attribute to exist
      };
    }

    // Attribute objects
    else if ("value" in data && "max" in data) {
      return {
        type: "bar",
        attribute: attr,
        value: Number(data.value || 0),
        max: Number(data.max || 0),
        min: Number(data.min || 0), // super does not have min values
        editable: true, // super requires a templated attribute to exist
      };
    }

    // Otherwise null
    return null;
  }

  /**
   * @typedef {object} TrackedAttributesDescription
   * @property {string[][]} bar    A list of property path arrays to attributes with both a value and a max property.
   * @property {string[][]} value  A list of property path arrays to attributes that have only a value property.
   */

  /**
   * @override
   * 
   * Retrieve an Array of attribute choices from a plain object.
   * @param {object} data  The object to explore for attributes.
   * @param {string[]} _path
   * @returns {TrackedAttributesDescription}
   * @protected
   */
  static _getTrackedAttributesFromObject(data, _path = []) {
    // Track the path and record found attributes
    const attributes = { bar: [], value: [] };

    const trackeddata = data.tracked || {};
    _path = ['tracked'];

    // Recursively explore the object
    for (let [k, v] of Object.entries(trackeddata)) {
      attributes.bar.push(_path.concat([k, "system"]));
    }

    /* We are not supporting single values yet but this is the starting point
    const dynamicdata = data.dynamic || {};
    _path = ['dynamic'];

    // Recursively explore the object
    for (let [k, v] of Object.entries(dynamicdata)) {
      attributes.value.push(_path.concat([k, "system.moddedvalue"]));
    }
    */

    return attributes;
  }
}

export class VsDToken extends Token {
  /**
   * Draw a single resource bar, given provided data
   * @param {number} number       The Bar number
   * @param {PIXI.Graphics} bar   The Bar container
   * @param {Object} data         Resource data for this bar
   * @protected
   */
  _drawBar(number, bar, data) {
    const val = Number(data.value);
    const min = Number(data.min); // super does not have min values
    const pct = Math.clamp(val, 0, data.max) / data.max; // pct controls the colour
    const pctfull = Math.clamp(val - min, 0, data.max - min) / (data.max - min); // pctfull controls the fill range

    // Determine sizing
    let h = Math.max(canvas.dimensions.size / 12, 8);
    const w = this.w;
    const bs = Math.clamp(h / 8, 1, 2);
    if (this.document.height >= 2) h *= 1.6; // Enlarge the bar for large tokens

    // Determine the color to use
    const blk = 0x000000;
    let color;
    if (val <= 0) color = 0x600000; // values less than zero are dark red
    else if (number === 0) color = Color.fromRGB([1 - pct / 2, pct, 0]);
    else color = Color.fromRGB([0.5 * pct, 0.7 * pct, 0.5 + pct / 2]);

    // Draw the bar
    bar.clear();
    bar.beginFill(blk, 0.5).lineStyle(bs, blk, 1.0).drawRoundedRect(0, 0, this.w, h, 3);
    bar.beginFill(color, 1.0).lineStyle(bs, blk, 1.0).drawRoundedRect(0, 0, pctfull * w, h, 2);

    // Set position
    let posY = number === 0 ? this.h - h : 0;
    bar.position.set(0, posY);
  }
}
export class TokenEffects {
  /**
   * Patch core methods
   */
  static patchCore() {
    Token.prototype._drawEffect = TokenEffects._drawEffect;
    TokenHUD.prototype._getStatusEffectChoices = TokenEffects._getStatusEffectChoices;
  }

  /**
   * Draw a status effect icon
   * @return {Promise<void>}
   * @private
   */
  static async _drawEffect(src, i, bg, w, tint) {
    const effectSize = game.settings.get("vsd", "effectSize");

    // Use the default values if no setting found
    const divisor = effectSize ? CONFIG.tokenEffects.effectSize[effectSize] : 5;
    const multiplier = 10 / divisor;

    // By default the width is multipled by 2, so divide by 2 first then use the new multiplier
    w = (w / 2) * multiplier;

    let tex = await loadTexture(src, { fallback: "icons/svg/hazard.svg" });
    let icon = this.hud.effects.addChild(new PIXI.Sprite(tex));
    icon.width = icon.height = w;
    //const nr = Math.floor(this.document.height * 5);
    const nr = Math.floor(this.document.height * divisor);
    icon.x = Math.floor(i / nr) * w;
    icon.y = (i % nr) * w;
    if (tint) icon.tint = tint;
    bg.drawRoundedRect(icon.x + 1, icon.y + 1, w - 2, w - 2, 2);
  }

  /**
   * Get an array of icon paths which represent valid status effect choices
   * @private
   */
  static _getStatusEffectChoices() {
    const token = this.object;
    const doc = token.document;

    // Get statuses which are active for the token actor
    const actor = token.actor || null;
    const statuses = actor ? actor.effects.reduce((obj, e) => {
      const id = e.getFlag("core", "statusId");
      if (id) {
        obj[id] = {
          id: id,
          overlay: !!e.getFlag("core", "overlay"),
        };
      }
      return obj;
    }, {}) : {};

    // Prepare the list of effects from the configured defaults and any additional effects present on the Token
    const tokenEffects = foundry.utils.deepClone(doc.effects) || [];
    if (doc.overlayEffect) tokenEffects.push(doc.overlayEffect);
    return CONFIG.statusEffects.concat(tokenEffects).reduce((obj, e) => {
      const src = e.icon ?? e;
      //if ( src in obj ) return obj; // Not sure why I commented this out
      const status = statuses[e.id] || {};
      const isActive = !!status.id || doc.effects.includes(src);
      const isOverlay = !!status.overlay || doc.overlayEffect === src;
      obj[src] = {
        id: e.id ?? "",
        title: e.label ? game.i18n.localize(e.label) : null,
        src,
        isActive,
        isOverlay,
        cssClass: [
          isActive ? "active" : null,
          isOverlay ? "overlay" : null,
        ].filterJoin(" "),
      };
      return obj;
    }, {});
  }
}
