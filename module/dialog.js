export class MyDialog extends Dialog {

  /**
   * @override
   * Handle a keydown event while the dialog is active
   * @param {KeyboardEvent} event   The keydown event
   * @private
   */
  _onKeyDown(event) {

    // Cycle Options
    if ( event.key === "Tab" || event.key === "Enter") {
      const dialog = this.element[0];

      // If we are already focused on the Dialog, let the default browser behavior take over
      if ( dialog.contains(document.activeElement) ) return;

      // If we aren't focused on the dialog, bring focus to one of its buttons
      event.preventDefault();
      event.stopPropagation();
      const dialogButtons = Array.from(document.querySelectorAll(".dialog-button"));
      const targetButton = event.shiftKey ? dialogButtons.pop() : dialogButtons.shift();
      targetButton.focus();
    }

    // Close dialog
    if ( event.key === "Escape" ) {
      event.preventDefault();
      event.stopPropagation();
      return this.close();
    }
  }
}
